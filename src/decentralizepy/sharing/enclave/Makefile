SGX_SDK := /opt/intel/sgxsdk
SGX_LIBRARY_PATH := $(SGX_SDK)/lib64

Enclave_Name := enclave.so
Signed_Enclave_Name := enclave.signed.so
Enclave_Cpp_File := enclave.cpp
Enclave_Cpp_Object := $(Enclave_Cpp_File:.cpp=.o)
Enclave_Include_Paths := -I$(SGX_SDK)/include -I$(SGX_SDK)/include/tlibc -I$(SGX_SDK)/include/libcxx
Enclave_C_Flags := -nostdinc -fvisibility=hidden -fpie -fstack-protector $(Enclave_Include_Paths)
Enclave_Cpp_Flags := $(Enclave_C_Flags) -nostdinc++
Enclave_Link_Flags := -Wl,--no-undefined -nostdlib -nodefaultlibs -nostartfiles -L$(SGX_LIBRARY_PATH) \
	-Wl,--whole-archive -lsgx_trts -Wl,--no-whole-archive \
	-Wl,--start-group -lsgx_tstdc -lsgx_tcxx -lsgx_tcrypto -lsgx_tservice -Wl,--end-group \
	-Wl,-Bstatic -Wl,-Bsymbolic -Wl,--no-undefined \
	-Wl,-pie,-eenclave_entry -Wl,--export-dynamic  \
	-Wl,--defsym,__ImageBase=0

all: build key sign

build:
	@sgx_edger8r --trusted ../enclave.edl --search-path $(SGX_SDK)/include
	@$(CC) -g -c $(Enclave_C_Flags) enclave_t.c -o enclave_t.o
	@$(CXX) -g -c $(Enclave_Cpp_Flags) $(Enclave_Cpp_File) -o $(Enclave_Cpp_Object)
	@$(CXX) -o $(Enclave_Name) enclave_t.o $(Enclave_Cpp_Object) $(Enclave_Link_Flags)

sign: 
	@sgx_sign sign -key private.pem -enclave $(Enclave_Name) -out $(Signed_Enclave_Name)

key:
	@openssl genrsa -out private.pem -3 3072

clean:
	@rm -f $(Enclave_Name) $(Signed_Enclave_Name) $(Enclave_Cpp_Object) enclave_t.* private.pem
