#include <string>

#ifdef __cplusplus
extern "C" {
struct lconv {
    char *decimal_point;
    char *thousands_sep;
    char *grouping;

    char *int_curr_symbol;
    char *currency_symbol;
    char *mon_decimal_point;
    char *mon_thousands_sep;
    char *mon_grouping;
    char *positive_sign;
    char *negative_sign;
    char int_frac_digits;
    char frac_digits;
    char p_cs_precedes;
    char p_sep_by_space;
    char n_cs_precedes;
    char n_sep_by_space;
    char p_sign_posn;
    char n_sign_posn;
    char int_p_cs_precedes;
    char int_p_sep_by_space;
    char int_n_cs_precedes;
    char int_n_sep_by_space;
    char int_p_sign_posn;
    char int_n_sign_posn;
};

static struct lconv *localeconv(void) {
    static struct lconv l;
    std::string point = ".";
    std::string negative_sign = "";
    l.decimal_point = l.mon_decimal_point = (char *) point.c_str();
    l.thousands_sep = l.grouping = l.int_curr_symbol = l.currency_symbol =
        l.mon_thousands_sep = l.mon_grouping = l.positive_sign =
            l.negative_sign = (char *) negative_sign.c_str();

    l.int_frac_digits = l.frac_digits = l.p_cs_precedes = l.p_sep_by_space =
        l.n_cs_precedes = l.n_sep_by_space = l.p_sign_posn = l.n_sign_posn =
            l.int_p_cs_precedes = l.int_p_sep_by_space = l.int_n_cs_precedes =
                l.int_n_sep_by_space = l.int_p_sign_posn = l.int_n_sign_posn =
                    127;

    return &l;
}

}
#endif

