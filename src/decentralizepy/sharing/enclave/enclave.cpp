#include "enclave_t.h"
#include "json.hpp"
#include <string>

using json = nlohmann::json;
#define MAX(x, y) (((x) > (y)) ? (x) : (y))

extern "C" {

    /**
     * @brief average models and copy the buffer to the initial pointer
     * 
     * @param buffer models and state dict
     */
    void average_enclave(char *buffer) {
        const json json_object = json::parse(buffer);

        const int n_neighbors = json_object["n_neighbors"].get<int>();

        const json state_dict = json_object["state_dict"];
        const json state_indices = json::parse(state_dict["indices"].get<std::string>());
        const json state_params = json::parse(state_dict["params"].get<std::string>());
        
        const int n_indices = state_indices.size();
        float *total_params = (float *) calloc(n_indices, sizeof(float));

        float weight_total = 0;
        int initialized = 0;

        for (auto& element : json_object.items()) {
            if (element.key() != "n_neighbors" && element.key() != "state_dict") {
                json data = element.value();
                int degree = data["degree"].get<int>();
                json indices = json::parse(data["indices"].get<std::string>());
                json params = json::parse(data["params"].get<std::string>());

                float weight = (float) 1 / (MAX(n_neighbors, degree) + 1);
                weight_total += weight;

                if (initialized == 0) {
                    for (int i = 0; i < n_indices; ++i) {
                        total_params[indices[i].get<int>()] = weight * params[i].get<float>();
                    }
                    initialized = 1;
                } else {
                    for (int i = 0; i < n_indices; ++i) {
                        total_params[indices[i].get<int>()] += weight * params[i].get<float>();
                    }
                }
            }
        }

        json total;

        for (int i = 0; i < n_indices; ++i) {
            total["indices"][i] = i;
            total["params"][state_indices[i].get<int>()] = total_params[state_indices[i].get<int>()] + (1 - weight_total) * state_params[i].get<float>();
        }

        total["indices"] = total["indices"].dump();
        total["params"] = total["params"].dump();

        const std::string json_str = total.dump();
        std::memcpy(buffer, json_str.c_str(), strlen(json_str.c_str()) + 1);

        free(total_params);
        total_params = NULL;
    }
}
