import json
import logging
import numpy
import torch
from os import path
from ctypes import *
from collections import deque

class Sharing:
    """
    API defining who to share with and what, and what to do on receiving

    """

    def __init__(
        self, rank, machine_id, communication, mapping, graph, model, dataset, log_dir
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data.
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        self.rank = rank
        self.machine_id = machine_id
        self.uid = mapping.get_uid(rank, machine_id)
        self.communication = communication
        self.mapping = mapping
        self.graph = graph
        self.model = model
        self.dataset = dataset
        self.communication_round = 0
        self.log_dir = log_dir
        self.total_data = 0
        self.c_program = None

        self.peer_deques = dict()
        self.my_neighbors = self.graph.neighbors(self.uid)
        for n in self.my_neighbors:
            self.peer_deques[n] = deque()

    def received_from_all(self):
        """
        Check if all neighbors have sent the current iteration

        Returns
        -------
        bool
            True if required data has been received, False otherwise

        """
        for _, i in self.peer_deques.items():
            if len(i) == 0:
                return False
        return True

    def get_neighbors(self, neighbors):
        """
        Choose which neighbors to share with

        Parameters
        ----------
        neighbors : list(int)
            List of all neighbors

        Returns
        -------
        list(int)
            Neighbors to share with

        """
        # modify neighbors here
        return neighbors

    def serialized_model(self):
        """
        Convert model to json dict. Here we can choose how much to share

        Returns
        -------
        dict
            Model converted to json dict

        """
        m = dict()
        for key, val in self.model.state_dict().items():
            m[key] = json.dumps(val.numpy().tolist())
            self.total_data += len(self.communication.encrypt(m[key]))
        return m

    def deserialized_model(self, m):
        """
        Convert received json dict to state_dict.

        Parameters
        ----------
        m : dict
            json dict received

        Returns
        -------
        state_dict
            state_dict of received

        """
        state_dict = dict()
        for key, value in m.items():
            state_dict[key] = torch.from_numpy(numpy.array(json.loads(value)))
        return state_dict

    def step(self, destroy):
        """
        Perform a sharing step. Implements D-PSGD.

        """
        serialized_model = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        iter_neighbors = self.get_neighbors(all_neighbors)
        serialized_model["degree"] = len(all_neighbors)
        serialized_model["iteration"] = self.communication_round
        for neighbor in iter_neighbors:
            self.communication.send(neighbor, serialized_model)

        logging.info("Waiting for messages from neighbors")
        while not self.received_from_all():
            sender, data = self.communication.receive()
            logging.debug("Received model from {}".format(sender))
            iteration = data["iteration"]
            self.peer_deques[sender].append(data)
            logging.info(
                "Deserialized received model from {} of iteration {}".format(
                    sender, iteration
                )
            )

        peer_data = dict()
        peer_data["n_neighbors"] = len(self.peer_deques)
        peer_data["state_dict"] = serialized_model
        for k, v in self.peer_deques.items():
            peer_data[k] = v.popleft()

        logging.info("Starting model averaging after receiving from all neighbors")
        
        # create enclave at first iteration
        if self.communication_round == 0:
            path_to_c_file = path.expanduser('~') + "/Gitlab/decentralizepy/src/decentralizepy/sharing/host/host.so"
            self.c_program = CDLL(path_to_c_file)
            self.c_program.initialize_enclave.argtype = None
            self.c_program.initialize_enclave.restype = c_int
            self.c_program.average.argtype = c_char_p
            self.c_program.average.restype = c_char_p
            self.c_program.destroy_enclave.argtype = None
            self.c_program.destroy_enclave.restype = c_int
            ret = self.c_program.initialize_enclave()
            if ret != 0:
                raise Exception("Failed to create the enclave")
            else:
                logging.info("Enclave successfully created")

        # call average function in the C++ enclave
        data = json.dumps(peer_data).encode("UTF-8")
        res = self.c_program.average(data).decode("UTF-8")
        total = self.deserialized_model(json.loads(res))

        # destroy enclave at last iteration
        if destroy:
            ret = self.c_program.destroy_enclave()
            if ret != 0:
                raise Exception("Failed to destroy the enclave")
            else:
                logging.info("Enclave successfully destroyed")

        self.model.load_state_dict(total)

        logging.info("Model averaging complete")

        self.communication_round += 1
