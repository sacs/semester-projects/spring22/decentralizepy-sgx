#include "sgx_urts.h"
#include "enclave_u.h"

extern "C" {
    
    sgx_enclave_id_t eid;

    /**
     * @brief create the enclave
     * 
     * @return int enclave creation status
     */
    int initialize_enclave() {
        sgx_status_t ret = SGX_SUCCESS;
        sgx_launch_token_t token = {0};
        int updated = 0;
        
        ret = sgx_create_enclave("/home/nadal/Gitlab/decentralizepy/src/decentralizepy/sharing/enclave/enclave.signed.so", SGX_DEBUG_FLAG, &token, &updated,&eid, NULL);

        if (ret != SGX_SUCCESS) {
            return -1;
        }

        return 0;
    }

    /**
     * @brief average the models
     * 
     * @param buffer models and state dict
     * @return char* averaged model
     */
    char* average(char* buffer)
    {   
        average_enclave(eid, buffer);
        return buffer;
    }

    /**
     * @brief destroy the enclave
     * 
     * @return int enclave destruction status
     */
    int destroy_enclave()
    {
        sgx_status_t ret = SGX_SUCCESS;

        ret = sgx_destroy_enclave(eid);

        if (ret != SGX_SUCCESS) {
            return -1;
        }
        
        return 0;
    }
}
